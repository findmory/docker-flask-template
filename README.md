

be sure default docker-machine is running
docker-machine ls

if you don't see default get it running with
docker-machine create (or using Kitematic)

also might have to run:
eval "$(docker-machine env default")

for dev run like this:
docker run -d --name flask-docker-test-container -p 80:80 -v $(pwd)/app:/app flask-docker-test-img python /app/main.py

that way you can edit code in local and it will update.  then when you are ready rebuild the image

docker build -t flask-docker-test-img .

create a new container
create the conatiner like this

docker run -d --name flask-docker-test-container -p 80:80 flask-docker-test-img

