from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "hello from docker!"

@app.route("/test")
def test():
    print('we are in the test route')
    return "this is the test route"

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, port=80)
