# start this with
# docker build -t flask-docker-test-img .

#FROM tiangolo/uwsgi-nginx:python2.7
FROM findmory/flask-img:latest

MAINTAINER Mike Ory <findmory@gmail.com>

RUN pip install flask

# Add app configuration to Nginx
COPY nginx.conf /etc/nginx/conf.d/

# Copy sample app
COPY ./app /app